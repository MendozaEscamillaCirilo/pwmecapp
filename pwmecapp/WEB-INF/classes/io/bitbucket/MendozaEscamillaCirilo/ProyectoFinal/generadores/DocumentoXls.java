package io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.generadores;


import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.servlets.ServletTextoCifrado;

//import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.servlets.ServletTextoCifrado;

public final class DocumentoXls {
	//private ServletTextoCifrado tc = new ServletTextoCifrado();
	public static void deLibros(HttpServletResponse response)
			throws IOException {
		
		HSSFWorkbook libroTrabajo = new HSSFWorkbook();
		HSSFSheet hoja = libroTrabajo.createSheet("Libros");
		String msg = ServletTextoCifrado.mensajec;
		String ms1 = ServletTextoCifrado.mensajet;
		
		/* Primera fila */
		int numeroFila = 0;
		HSSFRow fila = hoja.createRow(numeroFila);
		
		fila.createCell(0).setCellValue("Texto claro");
		fila.createCell(1).setCellValue("Texto cifrado");
		
		/* Siguiente fila */
		fila = hoja.createRow(numeroFila+1);
		fila.createCell(0).setCellValue(ms1);
		fila.createCell(1).setCellValue(msg);
		
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=mensaje_cifrado.xls");
		ServletOutputStream salida = response.getOutputStream();
		libroTrabajo.write(salida);
		
		libroTrabajo.close();
	
		 salida.close();
		
	}
	
}
