package io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.generadores;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;

import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.servlets.ServletTextoCifrado;

public final class DocumentoPdf {
	ServletTextoCifrado sc = new ServletTextoCifrado();
	public static void deLibros(HttpServletResponse response,String mensajec, String mensajet)
			throws IOException {
		try {
			String contenido = "TEXTO CLARO: "+mensajet+"\nMensaje cifrado: "+mensajec;
			try {
				
				//FileOutputStream archivo = new FileOutputStream(ruta);
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename=Mensaje_cifrado.pdf");
				ServletOutputStream archivo = response.getOutputStream();
				Document doc = new Document();
				PdfWriter.getInstance(doc, archivo);
				
				doc.open();
				doc.add(new Paragraph(contenido));
				doc.close();
				
			}catch (Exception e) {}
		}catch(Exception e) {}
	}
}
