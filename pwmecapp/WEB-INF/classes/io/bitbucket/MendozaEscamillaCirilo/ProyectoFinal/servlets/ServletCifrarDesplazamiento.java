package io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.servlets;

import java.io.IOException; 
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.utilerias.ChecaAlfabeto;
import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.utilerias.Desplazamiento;

@WebServlet(
		description = "Servlet para el procesamiento de un formulario de HTML", 
		urlPatterns = { 
				"/ServletCifrarDesplazamiento", 
				"/html-cifrar-desplazamiento"
		}
	)
	public class ServletCifrarDesplazamiento extends HttpServlet {
		private static final long serialVersionUID = 1L;
		private boolean uno = false;
		public static boolean esuno=false;
		public static String respuestacif="";
		public static String textocif="";
		public ServletCifrarDesplazamiento() {
	        super();
	    }
		private void mostrarFormularioHtml(
				HttpServletRequest solicitud, HttpServletResponse respuesta
		) throws IOException {
			respuesta.setContentType("text/html");
			respuesta.setCharacterEncoding("UTF-8");
			PrintWriter salida = respuesta.getWriter();
			StringBuffer documentoHtml = new StringBuffer();
			documentoHtml.append("<!DOCTYPE html>");
			documentoHtml.append("<html>");
			documentoHtml.append("<head>");
			documentoHtml.append("<meta charset=\"UTF-8\">");
			documentoHtml.append("<title>Desplazamiento - Cifrar</title>");
			documentoHtml.append("<link rel=\"stylesheet\" href=\"assets/css/normalize.css\">");
			documentoHtml.append("<link rel=\"stylesheet\" href=\"assets/css/main.css\">");
			documentoHtml.append("</head>");
			documentoHtml.append("<body>");
			documentoHtml.append("<div class=\"contenedor\">");
			documentoHtml.append("<div class=\"cabecera\">");
			documentoHtml.append("<h1>ServletCifrarDesplazamiento</h1>");
			documentoHtml.append("</div>");
			documentoHtml.append("<div class=\"cuerpo\">");
			documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			documentoHtml.append("<fieldset>");
			documentoHtml.append("<legend>CIFRAR TEXTO</legend>");
			/******************************************************************************************/
			documentoHtml.append("<div>");
			documentoHtml.append("<label for=\"input-text\">Text Claro</label>");
			documentoHtml.append("<p>");
			//documentoHtml.append("<input type=\"text\" id=\"input-text\" name=\"input-text\" value=\"\" placeholder=\"Ingresa texto a cifrar...\" />");
			documentoHtml.append("<textarea name=\"input-text\" cols=\"20\" rows=\"5\" placeholder=\"Ingresa texto a cifrar...\"></textarea>");
			documentoHtml.append("</p>");
			documentoHtml.append("</div>");
			
			documentoHtml.append("<div>");
			documentoHtml.append("<label for=\"input-number\">Desplazamiento</label>");
			documentoHtml.append("<p>");
			documentoHtml.append("<input type=\"number\" id=\"input-number\" name=\"input-number\" value=\"\"  placeholder=\"Rango entre -33 a 33...\" />");
			documentoHtml.append("</p>");
			documentoHtml.append("</div>");
			/******************************************************************************************/
			documentoHtml.append("<input type=\"submit\" value=\"Continuar\" class=\"enviar\" />");
			documentoHtml.append("</fieldset>");
			documentoHtml.append("</form>");
			documentoHtml.append("<div class=\"menu\">");
			documentoHtml.append("<ul>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarDesplazamiento\">ServletCifrarDesplazamiento</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarVigenere\">ServletCifrarVigenere</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletDescifrarDesplazamiento\">ServletDescifrarDesplazamiento</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletDescifrarVigenere\">ServletDescifrarVigenere</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarAtbash\">ServletCifrarAtbash</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarAtbash\">ServletDescifrarAtbash</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\".\">Inicio</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("</ul>");
			documentoHtml.append("</div>");
			documentoHtml.append("</div>");
			documentoHtml.append("</div>");
			documentoHtml.append("</body>");
			documentoHtml.append("</html>");
			salida.println(documentoHtml.toString());
		}
		
		private void procesarFormularioHtml1(
				HttpServletRequest solicitud, HttpServletResponse respuesta
		) throws IOException {
			uno = true;
			respuesta.setContentType("text/html");
			respuesta.setCharacterEncoding("UTF-8");
			solicitud.setCharacterEncoding("UTF-8");	
			PrintWriter salida = respuesta.getWriter();
			StringBuffer documentoHtml = new StringBuffer();
			documentoHtml.append("<!DOCTYPE html>");
			documentoHtml.append("<html>");
			documentoHtml.append("<head>");
			documentoHtml.append("<meta charset=\"UTF-8\">");
			documentoHtml.append("<title>Desplazamiento - Cifrar</title>");
			documentoHtml.append("<link rel=\"stylesheet\" href=\"assets/css/normalize.css\">");
			documentoHtml.append("<link rel=\"stylesheet\" href=\"assets/css/main.css\">");
			documentoHtml.append("</head>");
			documentoHtml.append("<body>");
			documentoHtml.append("<div class=\"contenedor\">");
			documentoHtml.append("<div class=\"cabecera\">");
			documentoHtml.append("<h1>ServletCifrarDesplazamiento</h1>");
			documentoHtml.append("</div>");
			documentoHtml.append("<div class=\"cuerpo\">");
			
			String input_text = ChecaAlfabeto.sinmenorque(solicitud.getParameter("input-text"));
			String input_number = solicitud.getParameter("input-number");
			Desplazamiento d = new Desplazamiento();
			int rango = 200;
			try {
				rango = Integer.parseInt(solicitud.getParameter("input-number"));
			}catch(Exception e){}
			
			if(input_number == null || input_number.equals(""))
			{
				documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			}else if(input_text == null || input_text.equals("")) {
				documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			}else if(rango < -33 || rango > 33){
				documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			}else if(input_text.length()>2000){
				documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			}else{
				documentoHtml.append("<form action=\"ServletTextoCifrado\" method=\"get\" class=\"formulario\">");
				documentoHtml.append("<div>");
				documentoHtml.append("<h3 id=\"bien\">Datos Correctos presiona CONTINUAR para obtener archivos</h3>");
				documentoHtml.append("<input type=\"text\" id=\"mensaje\" name=\"mensaje\" value=\"");
				documentoHtml.append(d.cifrar(input_text, Integer.parseInt(input_number))+"\"/>");
				documentoHtml.append("</div>");
				respuestacif = d.cifrar(input_text, Integer.parseInt(input_number));
				textocif = ChecaAlfabeto.sustituye(input_text);
				esuno=true;
			}
			documentoHtml.append("<fieldset>");
			documentoHtml.append("<legend>Cifrar</legend>");
			
			/******************************************************************************************/

			documentoHtml.append("<div>");
			documentoHtml.append("<label for=\"input-text\">Text Claro</label>");
			if(input_text == null || input_text.equals("")) {
				documentoHtml.append("<h3>INGRESA MENSAJE A CIFRAR</h3>");
				documentoHtml.append("<p>");
				documentoHtml.append("<textarea name=\"input-text\" cols=\"20\" rows=\"5\" placeholder=\"Ingresa texto a cifrar...\"></textarea>");
				documentoHtml.append("</p>");
			}else if(input_text.length() > 2000){
				documentoHtml.append("<h3>EL MENSAJE A CIFRAR TIENE MAS DE 2000 CARATERES</h3>");
				documentoHtml.append("<p>");
				documentoHtml.append("<textarea name=\"input-text\" cols=\"20\" rows=\"5\" placeholder=\"Ingresa texto a cifrar...\">");
				documentoHtml.append(input_text+" </textarea>");
				documentoHtml.append("</p>");
			}else{
				documentoHtml.append("<p>");
				documentoHtml.append("<textarea name=\"input-text\" cols=\"20\" rows=\"5\" placeholder=\"Ingresa texto a cifrar...\">");
				documentoHtml.append(input_text+" </textarea>");
				documentoHtml.append("</p>");
			}
			documentoHtml.append("</div>");
			documentoHtml.append("<div>");
			documentoHtml.append("<label for=\"input-number\">Desplazamiento</label>");
			
			if(input_number == null || input_number.equals("")) {
				documentoHtml.append("<h3>INGRESA UN NUMERO EN EL DESPLAZAMIENTO</h3>");
				documentoHtml.append("<p>");
				documentoHtml.append("<input type=\"number\" id=\"input-number\" name=\"input-number\" value=\"\" placeholder=\"Ingresa algo...\" />");
				documentoHtml.append("</p>");
			}else if((rango > 33) || (rango < -33)){
				documentoHtml.append("<h3>RANGO NO VALIDO</h3>");
				documentoHtml.append("<p>");
				documentoHtml.append("<input type=\"number\" id=\"input-number\" name=\"input-number\" value=\"\" placeholder=\"Ingresa algo...\" />");
				documentoHtml.append("</p>");
			}else {
				documentoHtml.append("<p>");
				documentoHtml.append("<input type=\"number\" id=\"input-number\" name=\"input-number\" value=\""+input_number+"\" placeholder=\" ");
				documentoHtml.append(input_number+"\" />");
				documentoHtml.append("</p>");
			}
			documentoHtml.append("</div>");

			/******************************************************************************************/
			
			documentoHtml.append("<input type=\"submit\" value=\"Continuar\" class=\"enviar\" />");
			documentoHtml.append("</fieldset>");
			documentoHtml.append("</form>");
			documentoHtml.append("<div class=\"menu\">");
			documentoHtml.append("<ul>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarDesplazamiento\">ServletCifrarDesplazamiento</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarVigenere\">ServletCifrarVigenere</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletDescifrarDesplazamiento\">ServletDescifrarDesplazamiento</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletDescifrarVigenere\">ServletDescifrarVigenere</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarAtbash\">ServletCifrarAtbash</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarAtbash\">ServletDescifrarAtbash</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\".\">Inicio</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("</ul>");
			documentoHtml.append("</div>");
			documentoHtml.append("</div>");
			documentoHtml.append("</div>");
			documentoHtml.append("</body>");
			documentoHtml.append("</html>");
			salida.println(documentoHtml.toString());
		}
		
		private void procesarFormularioHtml2(
				HttpServletRequest solicitud, HttpServletResponse respuesta
		) throws IOException {
			uno = false;
			respuesta.setContentType("text/html");
			respuesta.setCharacterEncoding("UTF-8");
			solicitud.setCharacterEncoding("UTF-8");			
			PrintWriter salida = respuesta.getWriter();
			StringBuffer documentoHtml = new StringBuffer();
			documentoHtml.append("<!DOCTYPE html>");
			documentoHtml.append("<html>");
			documentoHtml.append("<head>");
			documentoHtml.append("<meta charset=\"UTF-8\">");
			documentoHtml.append("<title>Desplazamiento - Cifrar</title>");
			documentoHtml.append("<link rel=\"stylesheet\" href=\"assets/css/normalize.css\">");
			documentoHtml.append("<link rel=\"stylesheet\" href=\"assets/css/main.css\">");
			documentoHtml.append("</head>");
			documentoHtml.append("<body>");
			documentoHtml.append("<div class=\"contenedor\">");
			documentoHtml.append("<div class=\"cabecera\">");
			documentoHtml.append("<h1>ServletCifrarDesplazamiento</h1>");
			documentoHtml.append("</div>");
			documentoHtml.append("<div class=\"cuerpo\">");
			
			String input_text = ChecaAlfabeto.sinmenorque(solicitud.getParameter("input-text"));
			String input_number = solicitud.getParameter("input-number");
			Desplazamiento d = new Desplazamiento();
			int rango = 200;
			try {
				rango = Integer.parseInt(solicitud.getParameter("input-number"));
			}catch(Exception e) {}
			
			if(input_number == null || input_number.equals(""))
			{
				documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			}else if(input_text == null || input_text.equals("")) {
				documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			}else if(rango < -33 || rango > 33){
				documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			}else if(input_text.length()>2000){
				documentoHtml.append("<form action=\"ServletCifrarDesplazamiento\" method=\"post\" class=\"formulario\">");
			}else{
				documentoHtml.append("<form action=\"ServletTextoCifrado\" method=\"get\" class=\"formulario\">");
				documentoHtml.append("<div>");
				documentoHtml.append("<h3 id=\"bien\">Datos Correctos presiona CONTINUAR para obtener archivos</h3>");
				documentoHtml.append("<input type=\"text\" id=\"mensaje\" name=\"mensaje\" value=\"");
				documentoHtml.append(d.cifrar(input_text, Integer.parseInt(input_number))+"\"/>");
				documentoHtml.append("</div>");
				respuestacif = d.cifrar(input_text, Integer.parseInt(input_number));
				textocif = ChecaAlfabeto.sustituye(input_text);
				esuno=true;
			}
			documentoHtml.append("<fieldset>");
			documentoHtml.append("<legend>Cifrar</legend>");
			
			/******************************************************************************************/

			documentoHtml.append("<div>");
			documentoHtml.append("<label for=\"input-text\">Text Claro</label>");
			if(input_text == null || input_text.equals("")) {
				documentoHtml.append("<h3>INGRESA MENSAJE A CIFRAR</h3>");
				documentoHtml.append("<p>");
				documentoHtml.append("<textarea name=\"input-text\" cols=\"20\" rows=\"5\" placeholder=\"Ingresa texto a cifrar...\"></textarea>");
				documentoHtml.append("</p>");
			}else  if(input_text.length() > 2000){
				documentoHtml.append("<h3>EL MENSAJE A CIFRAR TIENE MAS DE 2000 CARATERES</h3>");
				documentoHtml.append("<p>");
				documentoHtml.append("<textarea name=\"input-text\" cols=\"20\" rows=\"5\" placeholder=\"Ingresa texto a cifrar...\">");
				documentoHtml.append(input_text+" </textarea>");
				documentoHtml.append("</p>");
			}else{
				documentoHtml.append("<p>");
				documentoHtml.append("<textarea name=\"input-text\" cols=\"20\" rows=\"5\" placeholder=\"Ingresa texto a cifrar...\">");
				documentoHtml.append(input_text+" </textarea>");
				documentoHtml.append("</p>");
			}
			documentoHtml.append("</div>");
			documentoHtml.append("<div>");
			documentoHtml.append("<label for=\"input-number\">Desplazamiento</label>");
			
			if(input_number == null || input_number.equals("")) {
				documentoHtml.append("<h3>INGRESA UN NUMERO EN EL DESPLAZAMIENTO</h3>");
				documentoHtml.append("<p>");
				documentoHtml.append("<input type=\"number\" id=\"input-number\" name=\"input-number\" value=\"\" placeholder=\"Ingresa algo...\" />");
				documentoHtml.append("</p>");
			}else if((rango > 33) || (rango < -33)){
				documentoHtml.append("<h3>RANGO NO VALIDO</h3>");
				documentoHtml.append("<p>");
				documentoHtml.append("<input type=\"number\" id=\"input-number\" name=\"input-number\" value=\"\" placeholder=\"Ingresa algo...\" />");
				documentoHtml.append("</p>");
			}else {
				documentoHtml.append("<p>");
				documentoHtml.append("<input type=\"number\" id=\"input-number\" name=\"input-number\" value=\""+input_number+"\" placeholder=\" ");
				documentoHtml.append(input_number+"\" />");
				documentoHtml.append("</p>");
			}
			documentoHtml.append("</div>");

			/******************************************************************************************/
			
			documentoHtml.append("<input type=\"submit\" value=\"Continuar\" class=\"enviar\" />");
			documentoHtml.append("</fieldset>");
			documentoHtml.append("</form>");
			documentoHtml.append("<div class=\"menu\">");
			documentoHtml.append("<ul>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarDesplazamiento\">ServletCifrarDesplazamiento</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarVigenere\">ServletCifrarVigenere</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletDescifrarDesplazamiento\">ServletDescifrarDesplazamiento</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletDescifrarVigenere\">ServletDescifrarVigenere</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarAtbash\">ServletCifrarAtbash</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\"ServletCifrarAtbash\">ServletDescifrarAtbash</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("<p><li>");
			documentoHtml.append("<a href=\".\">Inicio</a>");
			documentoHtml.append("</li></p>");
			documentoHtml.append("</ul>");
			documentoHtml.append("</div>");
			documentoHtml.append("</div>");
			documentoHtml.append("</div>");
			documentoHtml.append("</body>");
			documentoHtml.append("</html>");
			salida.println(documentoHtml.toString());
		}
		
		protected void doGet(
				HttpServletRequest request, HttpServletResponse response
		) throws ServletException, IOException {
			this.mostrarFormularioHtml(request, response);
		}

		protected void doPost(
				HttpServletRequest request, HttpServletResponse response
		) throws ServletException, IOException {
			if(uno) {
				this.procesarFormularioHtml2(request, response);
			}else {
				this.procesarFormularioHtml1(request, response);
			}
			
		}
		public static void setEsuno() {
			esuno=false;
		}
	}
