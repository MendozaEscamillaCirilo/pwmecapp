package io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.generadores.DocumentoPdf;
import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.generadores.DocumentoXls;

@WebServlet(
	urlPatterns = {
		"/ServletImpresionDocumento",
		"/impresion-de-documento"
	}
)
public class ServletImpresionDocumento extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public ServletImpresionDocumento() {
		super();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String formatoSolicitado = request.getParameter("formato");
		if (formatoSolicitado == null) {
			formatoSolicitado = "";
		}
		switch (formatoSolicitado) {
		case "xls":
			/* Microsoft Excel Worksheet */
			DocumentoXls.deLibros(response);
			break;
		case "pdf":
			/* PDF Document  */
			DocumentoPdf.deLibros(response,ServletTextoCifrado.mensajec,ServletTextoCifrado.mensajet);
			break;
		default:
			/* HTML */
			//DocumentoHtml.deError(response);
			break;
		}
		
	}
	
}
