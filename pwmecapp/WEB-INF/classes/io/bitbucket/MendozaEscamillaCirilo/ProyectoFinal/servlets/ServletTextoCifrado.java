package io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.utilerias.ChecaAlfabeto;

@WebServlet("/ServletTextoCifrado")
public class ServletTextoCifrado extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String mensajec = "";
	public static String mensajet = "";
    public ServletTextoCifrado() {
        super();
    }

    private void mostrarFormularioHtml(
			HttpServletRequest solicitud, HttpServletResponse respuesta
	) throws IOException {
		respuesta.setContentType("text/html");
		respuesta.setCharacterEncoding("UTF-8");
		PrintWriter salida = respuesta.getWriter();
		StringBuffer documentoHtml = new StringBuffer();
		documentoHtml.append("<!DOCTYPE html>");
		documentoHtml.append("<html>");
		documentoHtml.append("<head>");
		documentoHtml.append("<meta charset=\"UTF-8\">");
		documentoHtml.append("<title>Desplazamiento - Cifrar</title>");
		documentoHtml.append("<link rel=\"stylesheet\" href=\"assets/css/normalize.css\">");
		documentoHtml.append("<link rel=\"stylesheet\" href=\"assets/css/main.css\">");
		documentoHtml.append("</head>");
		documentoHtml.append("<body>");
		documentoHtml.append("<div class=\"contenedor\">");
		documentoHtml.append("<div class=\"cabecera\">");
		documentoHtml.append("<h1>ServletCifrarDesplazamiento</h1>");
		documentoHtml.append("</div>");
		documentoHtml.append("<div class=\"cuerpo\">");
		
		boolean d1 = ServletCifrarDesplazamiento.esuno;
		boolean d2 = ServletCifrarVigenere.esuno;
		boolean d3 = ServletDescifrarDesplazamiento.esuno;
		boolean d4 = ServletDescifrarVigenere.esuno;
		boolean d5 = ServletCifrarAtbash.esuno;
		
		
		if(d1==true) {
			mensajec = ServletCifrarDesplazamiento.respuestacif;
			mensajet = ServletCifrarDesplazamiento.textocif;
			ServletCifrarDesplazamiento.setEsuno();
		}else if(d2==true) {
			mensajec = ServletCifrarVigenere.respuestacif;
			mensajet = ServletCifrarVigenere.textocif;
			ServletCifrarVigenere.setEsuno();
		}else if(d3==true) {
			mensajec = ServletDescifrarDesplazamiento.respuestacif;
			mensajet = ServletDescifrarDesplazamiento.textocif;
			ServletDescifrarDesplazamiento.setEsuno();
		}else if(d4==true) {
			mensajec = ServletDescifrarVigenere.respuestacif;
			mensajet = ServletDescifrarVigenere.textocif;
			ServletDescifrarVigenere.setEsuno();
		}else if(d5==true) {
			mensajec = ServletCifrarAtbash.respuestacif;
			mensajet = ServletCifrarAtbash.textocif;
			ServletCifrarAtbash.setEsuno();
		}
		
		documentoHtml.append("<h2>Texto Cifrado</h2>");
		documentoHtml.append("<fieldset>");
		documentoHtml.append("<legend>Texto Cifrado</legend>");
		documentoHtml.append("<h4>" + ChecaAlfabeto.sinmenorque(mensajec) + "</h4>");
		documentoHtml.append("</fieldset>");
		documentoHtml.append("<fieldset>");
		documentoHtml.append("<legend>Texto Claro</legend>");
		
		/******************************************************************************************/

		documentoHtml.append("<div>");
		documentoHtml.append("<pre>");
		documentoHtml.append(ChecaAlfabeto.sinmenorque(mensajet));
		documentoHtml.append("</pre>");
		documentoHtml.append("</div>");

		/******************************************************************************************/
		documentoHtml.append("</fieldset>");
		
		documentoHtml.append("<div class=\"menu\">");
		documentoHtml.append("<h4>Obtener archivo con los siguientes formatos</h4>");
		documentoHtml.append("<a href=\"serializacion-de-datos?formato=csv\" class=\"un\">CVS</a>   |");
		documentoHtml.append("|   <a href=\"serializacion-de-datos?formato=xml\" class=\"un\">XML</a>   |");
		documentoHtml.append("|   <a href=\"serializacion-de-datos?formato=json\" class=\"un\">JSON</a>   |");
		documentoHtml.append("|   <a href=\"impresion-de-documento?formato=pdf\" class=\"un\">PDF</a>   |");
		documentoHtml.append("|   <a href=\"impresion-de-documento?formato=xls\" class=\"un\">Excel</a>");
		documentoHtml.append("<ul>");
		documentoHtml.append("<p><li>");
		documentoHtml.append("<a href=\"ServletCifrarDesplazamiento\">ServletCifrarDesplazamiento</a>");
		documentoHtml.append("</li></p>");
		documentoHtml.append("<p><li>");
		documentoHtml.append("<a href=\"ServletCifrarVigenere\">ServletCifrarVigenere</a>");
		documentoHtml.append("</li></p>");
		documentoHtml.append("<p><li>");
		documentoHtml.append("<a href=\"ServletDescifrarDesplazamiento\">ServletDescifrarDesplazamiento</a>");
		documentoHtml.append("</li></p>");
		documentoHtml.append("<p><li>");
		documentoHtml.append("<a href=\"ServletDescifrarVigenere\">ServletDescifrarVigenere</a>");
		documentoHtml.append("</li></p>");
		documentoHtml.append("<p><li>");
		documentoHtml.append("<a href=\"ServletCifrarAtbash\">ServletCifrarAtbash</a>");
		documentoHtml.append("</li></p>");
		documentoHtml.append("<p><li>");
		documentoHtml.append("<a href=\"ServletCifrarAtbash\">ServletDescifrarAtbash</a>");
		documentoHtml.append("</li></p>");
		documentoHtml.append("<p><li>");
		documentoHtml.append("<a href=\".\">Inicio</a>");
		documentoHtml.append("</li></p>");
		documentoHtml.append("</ul>");
		documentoHtml.append("</div>");
		documentoHtml.append("</div>");
		documentoHtml.append("</div>");
		documentoHtml.append("</body>");
		documentoHtml.append("</html>");
		
		salida.println(documentoHtml.toString());
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.mostrarFormularioHtml(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}
	
}
