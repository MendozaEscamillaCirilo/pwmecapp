package io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.servlets;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.servlets.ServletTextoCifrado;

@WebServlet(
	urlPatterns = {
		"/ServletSerializacionDatosTextoPlano",
		"/serializacion-de-datos"
	}
)
public class ServletSerializacionDatosTextoPlano extends HttpServlet {

	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String formatoSolicitado = request.getParameter("formato");
		if (formatoSolicitado == null) {
			formatoSolicitado = "";
		}
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter salida = response.getWriter();
		
		//String resp = request.getParameter("input-text");
		//String res1 = request.getParameter("input-number");
		String msgcif = ServletTextoCifrado.mensajec;
		String msgnor = ServletTextoCifrado.mensajet;
		String salto1[] = msgcif.split("\n");
		String salto2[] = msgnor.split("\n");
		
		switch (formatoSolicitado) {
		case "csv":
			response.setContentType("text/csv");
			response.setHeader("Content-Disposition", "inline; filename=\"mensaje.csv\"");
			salida.println("Mensaje claro : "+ msgnor +"\nMensaje cifrado : "+msgcif+"\n");
			break;
		case "xml":
			response.setContentType("application/xml");
			response.setHeader("Content-Disposition", "inline; filename=\"mensaje.xml\"");
			if(salto1.length==1) {
				salida.println("<datos><texto_claro>"+msgnor+"</texto_claro>"
						+ "<texto_cifrado>"+msgcif+"</texto_cifrado></datos>");
			}else {
				String completo = "<datos><texto_claro>";
				for (int i = 0; i < salto1.length; i++) {
					completo+="<salto"+i+">"+salto1[i]+"</salto"+i+">";
				}
				completo+="</texto_claro><texto_cifrado>";
				for (int i = 0; i < salto2.length; i++) {
					completo+="<salto"+i+">"+salto2[i]+"</salto"+i+">";
				}
				completo+="</texto_cifrado></datos>";
				salida.println(completo);
			}
			break;
		case "json":
			response.setContentType("application/json");
			response.setHeader("Content-Disposition", "inline; filename=\"mensaje.json\"");
			if(salto1.length==1) {
				salida.println("{\"texto_claro\":\""+msgnor+"\",\"texto_cifrado\":\""+msgcif+"\"}");
			}else {
				String completo = "{\"texto_claro\": [";
				for (int i = 0; i < salto1.length; i++) {
					String aux="";
					if(i==salto1.length-1) {
						aux=salto1[i];
					}else{
						aux=salto1[i].substring(0,salto1[i].length()-1);
					}
					completo+="\""+aux+"\""+((i==salto1.length-1) ? "" : ",");
				}
				completo+="],\"texto_cifrado\" : [";
				for (int i = 0; i < salto2.length; i++) {
					String aux="";
					if(i==salto2.length-1) {
						aux=salto2[i];
					}else{
						aux=salto2[i].substring(0,salto2[i].length()-1);
					}
					completo+="\""+aux+"\""+((i==salto2.length-1) ? "\n" : ",\n");
				}
				completo+="]\n}";
				System.out.println(completo);
				salida.println(completo);
			}
			
			break;
		default:
			response.setContentType("text/html");
			salida.println("Error");
			break;
		}
		
	}

}