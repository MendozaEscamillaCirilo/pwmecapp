package io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.utilerias;

public class Vigenere {
	String arr[] = {"a","�","b","c","d","e","�","f","g","h","i","�","j","k","l","m","n","�","o","�","p","q","r","s","t","u","�","�","v","w","x","y","z"};
	int aux[];
	int l = 33;
	public String cifrar(String men,String cla) {
		aux = new int[men.length()];
		/****************************/
		for(int i=0;i<men.length();i++) {
			if(Character.isUpperCase(men.charAt(i))){ aux[i] = 1; } else { aux[i] = 0; }
		}
		/*************************/
		men=men.toLowerCase();
		cla=cla.toLowerCase();
		String mensaje = "";
		
		if(men.length() == cla.length() || men.length() < cla.length()) {
			for(int i=0;i<men.length();i++) {
				if(dameLugar(men.charAt(i)+"")==100) {
					if((men.charAt(i)+"").equals("<")) {
						mensaje+="&lt;";
					}else if((men.charAt(i)+"").equals(">")) {
						mensaje+="&gt;";
					}else {
						mensaje+=men.charAt(i)+"";
					}
				}else {
					mensaje+=arr[(dameLugar(men.charAt(i)+"") + dameLugar(cla.charAt(i)+""))%l];
				}
			}
		}else{
			int con=0;
			int tot = cla.length();
			int lugar = 0;
			for(int i=0;i<men.length();i++) {
				if(dameLugar(men.charAt(i)+"")==100) {
					mensaje+=men.charAt(i)+"";
				}else {
					lugar = dameLugar(men.charAt(i)+"") + dameLugar(cla.charAt(con%tot)+"");
					mensaje+=arr[lugar%l];
					con++;
				}
			}
		}
		
		String maux = mensaje;
		mensaje = "";
		for(int i=0;i<maux.length();i++) {
			if(aux[i]==1){
				mensaje+=(maux.charAt(i)+"").toUpperCase();
				}else {
					mensaje+=maux.charAt(i);
				}
		}
		return mensaje;
	}
	
	/********************************************************************************************************************/
	public String descifrar(String men,String cla) {
		aux = new int[men.length()];
		/****************************/
		for(int i=0;i<men.length();i++) {
			if(Character.isUpperCase(men.charAt(i))){ aux[i] = 1; } else { aux[i] = 0; }
		}
		/*************************/
		men=men.toLowerCase();
		cla=cla.toLowerCase();
		String mensaje = "";
		int pos = -1;
		
		if (men.length()==cla.length() || men.length()< cla.length()) {
			for (int i = 0; i < men.length(); i++) {
				if ((dameLugar(men.charAt(i)+"")) == 100){
					if ((men.charAt(i)+"").equals("&")){
						if ((men.charAt(i+1)+"").equals("l")){
							mensaje+="<";
							i+=3;
						}else{
							mensaje+=">";
							i+=3;
						}
					}else{
						mensaje+=men.charAt(i)+"";
					}	
				}else{
					pos = dameLugar(men.charAt(i)+"") + dameLugar(cla.charAt(i)+"");
					pos = pos%l;
					mensaje+=arr[pos];
				}
			}
		}else{
			int j = cla.length();
			int c = 0;
			for (int i = 0; i < men.length(); i++) {
				if ((dameLugar(men.charAt(i)+"")) == 100){
					if ((men.charAt(i)+"").equals("&")){
						if ((men.charAt(i+1)+"").equals("l")){
							mensaje+="<";
							i+=3;
						}else{
							mensaje+=">";
							i+=3;
						}
					}else{
						mensaje+=men.charAt(i)+"";
					}	
				}else {
					pos = dameLugar(men.charAt(i)+"") - dameLugar(cla.charAt(c%j)+"");
					pos = pos % l;
					c++;
					if(pos < 0) {
						pos = pos + l;
						mensaje+=arr[pos];
					}else {
						mensaje+=arr[pos];
					}
				}
			}
		}
		
		
		/****PARA REGRESAR A MAYUSCULAS*/
		String maux = mensaje;
		mensaje = "";
		for(int i=0;i<maux.length();i++) {
			if(aux[i]==1){
				mensaje+=(maux.charAt(i)+"").toUpperCase();
				}else {
					mensaje+=maux.charAt(i);
				}
		}
		return mensaje;
	}
	/*****************************************************************************************************************/
	
	private int dameLugar(String s){
		for(int i=0;i<arr.length;i++) {
			if(arr[i].equals(s))
				return i;
		}
		return 100;
	}
}
