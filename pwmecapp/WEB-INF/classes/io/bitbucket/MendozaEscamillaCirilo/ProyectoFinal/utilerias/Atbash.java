package io.bitbucket.MendozaEscamillaCirilo.ProyectoFinal.utilerias;

public class Atbash {
	static String arr[] = {"a","�","b","c","d","e","�","f","g","h","i","�","j","k","l","m","n","�","o","�","p","q","r","s","t","u","�","�","v","w","x","y","z"};
	static int aux[];
	public static String cifrar(String men) {
		if(men == null) {return "";}
		String mensaje = "";
		aux = new int[men.length()];
/*************************************************************************************************************************/
		for(int i=0;i<men.length();i++) {
			if(Character.isUpperCase(men.charAt(i))){ aux[i] = 1; } else { aux[i] = 0; }
		}
/*************************************************************************************************************************/
		for (int i = 0; i < men.length(); i++) {
			if(dameLugar(men.charAt(i)+"")==100) {
				if((men.charAt(i)+"").equals("&")) {
					if((men.charAt(i+1)+"").equals("l")) {
						mensaje+="<";
						i+=3;
					}else {
						mensaje+=">";
						i+=3;
					}
				}else {
					mensaje+=men.charAt(i)+"";
				}
			}else {
				mensaje+=cif(men.charAt(i)+"");
			}
		}
	String menaux = mensaje;
	mensaje = "";
	for (int i = 0; i < men.length(); i++) {
		if(aux[i] == 1) {
			mensaje+=(menaux.charAt(i)+"").toUpperCase();
		}else {
			mensaje+=menaux.charAt(i)+"";
		}
	}
		return mensaje;
	}
	
	private static String cif(String s) {
		int a1 = dameLugar(s);
		int a2 = 33 - a1-1;
		return arr[a2];
	}
	
	private static int dameLugar(String s){
		for(int i=0;i<arr.length;i++) {
			if(arr[i].equals(s))
				return i;
		}
		return 100;
	}
}
